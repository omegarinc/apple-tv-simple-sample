//
//  Movie.swift
//  Popular-movies-tvOs
//
//  Created by Andrey on 13.05.16.
//  Copyright © 2016 runningrecords. All rights reserved.
//

import Foundation

class Movie {
  
  let URL_BASE = "http://image.tmdb.org/t/p/w500"
  
  var title: String!
  var overview: String!
  var posterPath:String!
  var voteAverage:Double!
  var releaseDate:NSString!
  
  var isForAdults = false
  
  
  init(movieDict: Dictionary<String, AnyObject>) {
    if let title = movieDict["title"] as? String {
      self.title = title
    }
    
    if let overview = movieDict["overview"] as? String {
      self.overview = overview
    }
    
    if let posterPath = movieDict["poster_path"] as? String {
      self.posterPath = "\(URL_BASE)\(posterPath)"
    }
    
    
    if let releaseDate = movieDict["release_date"] as? String {
      self.releaseDate = releaseDate
    }
    
    if let voteAverage = movieDict["vote_average"] as? Double {
      self.voteAverage = voteAverage
    }
    
    if let isForAdults = movieDict["adult"] as? Bool {
      self.isForAdults = isForAdults
    }
  }
  
}
//
//  PlayerViewController.swift
//  Popular-movies-tvOs
//
//  Created by Andrey on 18.05.16.
//  Copyright © 2016 runningrecords. All rights reserved.
//

import UIKit
import AVKit

class PlayerViewController: AVPlayerViewController {
  
  override func viewDidLoad() {
     super.viewDidLoad()
    player = AVPlayer(URL: NSURL(string: "http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_5mb.mp4")!)
    player?.play()
  }

}

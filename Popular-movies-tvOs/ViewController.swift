 //
//  ViewController.swift
//  Popular-movies-tvOs
//
//  Created by Andrey on 13.05.16.
//  Copyright © 2016 runningrecords. All rights reserved.
//
 

 
import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

  
  @IBOutlet weak var collectionView: UICollectionView!
  var movies = [Movie]()
  let URL_BASE = "http://api.themoviedb.org/3/movie/popular?api_key=ebea8cfca72fdff8d2624ad7bbf78e4c";
  let defaultSize = CGSizeMake(311, 427)
  let focusSize = CGSizeMake(342, 469)
  var movieDataSelect: Movie? = nil
  
  
  override func viewDidLoad() {
    super.viewDidLoad()

    collectionView.delegate = self
    collectionView.dataSource = self
    
    if self.movies.count == 0 {
      downloadData()

    }
  }
  
  func downloadData() {
    let url = NSURL(string: URL_BASE)!
    let request = NSURLRequest(URL: url)
    let session = NSURLSession.sharedSession()

    let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
      
      if error != nil {
        
        print(error.debugDescription)
      } else {
          do {
            let dict = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? Dictionary<String, AnyObject>
            if let results = dict!["results"] as? [Dictionary<String, AnyObject>] {
              
              for obj in results {
                let movie = Movie(movieDict: obj)
                self.movies.append(movie)
              }
              
              dispatch_async(dispatch_get_main_queue()) {
                self.collectionView.reloadData()
              }
              
              
            }
          } catch {
        
          }
        }
      
      }
    task.resume()
    }
  
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
    if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("MovieCell", forIndexPath: indexPath) as? MovieCell {
      let movie = movies[indexPath.row]
      cell.configureCell(movie)
      
      return cell
    } else {
        return MovieCell()
    }
  }

  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return movies.count
  }
  override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
    
    if let prev = context.previouslyFocusedView as? MovieCell {
      UIView.animateWithDuration(0.1, animations: { () -> Void in
        prev.movieImg.frame.size = self.defaultSize
      })
      
      if let prev = context.nextFocusedView as? MovieCell {
        UIView.animateWithDuration(0.1, animations: { () -> Void in
          prev.movieImg.frame.size = self.focusSize
        })
      }

    }
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return CGSizeMake(343, 535)
  }
  
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    self.movieDataSelect = movies[indexPath.row]
    
    performSegueWithIdentifier("ShowFilm", sender: self)
    
    
  }
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard (segue.identifier != nil) else {
      print("Warning! User profile handler doesn't exist");
      return;
    }
    
    switch segue.identifier! {
    case "ShowFilm":
      let destVC: ViewControllerFilmInfo = segue.destinationViewController as! ViewControllerFilmInfo;
      
      destVC.movie = movieDataSelect;
      destVC.movies = movies
      
    default: break
    }
  }

  
  
  
  
}


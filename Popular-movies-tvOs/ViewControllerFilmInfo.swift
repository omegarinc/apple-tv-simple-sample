//
//  ViewControllerFilmInfo.swift
//  Popular-movies-tvOs
//
//  Created by Andrey on 16.05.16.
//  Copyright © 2016 runningrecords. All rights reserved.
//

import UIKit
import Foundation


class ViewControllerFilmInfo: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var imageViewBg: UIImageView!
  
  @IBOutlet weak var labelTitle: UILabel!
  @IBOutlet weak var labelOverview: UILabel!
  
  @IBOutlet weak var labelScore: UILabel!
  @IBOutlet weak var labelScoreValue: UILabel!
  
  @IBOutlet weak var labelRating: UILabel!
  @IBOutlet weak var labelRatingValue: UILabel!
  
  @IBOutlet weak var labelReleaseDate: UILabel!
  @IBOutlet weak var labelReleaseDateValue: UILabel!
  
  @IBOutlet weak var collectionViewCard: UICollectionView!
  
  var movie: Movie!
  var movies = [Movie]()
  var movieDataSelect: Movie? = nil
  
  let defaultSizeCard = CGSizeMake(132, 162)
  let focusSizeCard = CGSizeMake(145, 178)
  

  override func viewDidLoad() {
    super.viewDidLoad()
    
    collectionViewCard.delegate = self
    collectionViewCard.dataSource = self
    
    refreshLabel()
    loadImg(true)
  
  }
  
  override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
    
    if let prev = context.previouslyFocusedView as? MovieCardCell {
      UIView.animateWithDuration(0.1, animations: { () -> Void in
        prev.movieImg.frame.size = self.defaultSizeCard
      })
      
      if let prev = context.nextFocusedView as? MovieCardCell {
        UIView.animateWithDuration(0.1, animations: { () -> Void in
          prev.movieImg.frame.size = self.focusSizeCard
        })
      }
      
    }
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return CGSizeMake(182, 217)
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
    if let cell = collectionViewCard.dequeueReusableCellWithReuseIdentifier("MovieCardCell", forIndexPath: indexPath) as? MovieCardCell {
      let movie = movies[indexPath.row]
      cell.configureCardCell(movie)
      
      return cell
    } else {
      return MovieCardCell()
    }
  }
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return movies.count
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    
    self.movie = movies[indexPath.row]
    refreshLabel()
    loadImg(false)
    
    
  }

  func refreshLabel() {
    
    self.labelTitle.text = movie.title
    self.labelOverview.text = movie.overview
    self.labelScoreValue.text = "\(movie.voteAverage)"
    self.labelRatingValue.text = movie.isForAdults ? "R" : "G"
    
    
    if let date = self.movie.releaseDate as? String {
      let formatter = NSDateFormatter()
      formatter.dateFormat =  "yyyy-MM-dd"// "MMM dd, yyyy"
      
      let dateDate = formatter.dateFromString(date)!
      formatter.dateFormat =  "MMM dd, yyyy"
      let dateString = formatter.stringFromDate(dateDate)
      
      
      self.labelReleaseDateValue.text = "\(dateString)"
      
    }
    //    self.labelReleaseDateValue.text
    
    

  }
  func loadImg(isOne: Bool) {
    if let path = movie.posterPath {
      let url = NSURL(string: path)!
      
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        
        let data = NSData(contentsOfURL: url)!
        
        dispatch_async(dispatch_get_main_queue()) {
          let img = UIImage(data: data)
          self.imageView.image = img
          
          self.imageViewBg.image = img
          let blurEffect = UIBlurEffect(style: .Dark)
          let blurEffectView = UIVisualEffectView(effect: blurEffect)
          blurEffectView.frame = self.view.bounds
          //          self.imageViewBg.removeFromSuperview()
          
          if isOne {
            self.imageViewBg.addSubview(blurEffectView)
          }
          
        }
      }
    }
  }
  @IBAction func playVideo(sender: AnyObject) {
    let vc = PlayerViewController()
    self.presentViewController(vc, animated: true, completion: nil)
  }
  
}






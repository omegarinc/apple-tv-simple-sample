//
//  ServiceProvider.swift
//  TopShelf
//
//  Created by Andrey on 16.05.16.
//  Copyright © 2016 runningrecords. All rights reserved.
//

import Foundation
import TVServices


class ServiceProvider: NSObject, TVTopShelfProvider {

   let URL_BASE = "http://api.themoviedb.org/3/movie/popular?api_key=ebea8cfca72fdff8d2624ad7bbf78e4c";
  
   let URL_BASE_SERVICE = "http://image.tmdb.org/t/p/w500"
  
  override init() {
      super.init()
  }

  // MARK: - TVTopShelfProvider protocol

  var topShelfStyle: TVTopShelfContentStyle {
      // Return desired Top Shelf style.
      return .Sectioned
  }

//  var topShelfItems: [TVContentItem] {
//      // Create an array of TVContentItems.
//      return []
//  }
  
  var topShelfItems: [TVContentItem] {
    // Create an array of TVContentItems.
    let wrapperID = TVContentIdentifier(identifier: "shelf-wrapper", container: nil)!
    let wrapperItem = TVContentItem(contentIdentifier: wrapperID)!
    var ContentItems = [TVContentItem]()
    
    let semaphore = dispatch_semaphore_create(0)
    
    
    let url = NSURL(string: URL_BASE)!
    let request = NSURLRequest(URL: url)
    let session = NSURLSession.sharedSession()
    
    let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
      
      if error != nil {
        
        print(error.debugDescription)
      } else {
        do {
          let dict = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as? Dictionary<String, AnyObject>
          if let results = dict!["results"] as? [Dictionary<String, AnyObject>] {
            
            for obj in results {
              
              let identifier = TVContentIdentifier(identifier: "VOD", container: wrapperID)!
              let contentItem = TVContentItem(contentIdentifier: identifier )!
              
              if let title = obj["title"] as? String {
                contentItem.title = title
              }
              
              if let posterPath = obj["poster_path"] as? String {
                contentItem.imageURL = NSURL(string: "\(self.URL_BASE_SERVICE)\(posterPath)")
              }
              contentItem.imageShape = .Poster
              ContentItems.append(contentItem)
            }
            
          }
        } catch {
          
        }
        
      }
      
      wrapperItem.title = "Featured Movies"
      wrapperItem.topShelfItems = ContentItems
      
      dispatch_semaphore_signal(semaphore)
      
//      return [wrapperItem]

      
    }
    task.resume()
    
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
    return [wrapperItem]

    
    
    
    
    
    
//    for (var i = 0; i < 8; i++) {
//    
//      let identifier = TVContentIdentifier(identifier: "VOD", container: wrapperID)!
//      let contentItem = TVContentItem(contentIdentifier: identifier )!
//    
//      if let url = NSURL(string: "http://www.brianjcoleman.com/code/images/feature-\(i).jpg") {
//      
//        contentItem.imageURL = url
//        contentItem.imageShape = .HDTV
//        contentItem.title = "Movie Title"
//        contentItem.displayURL = NSURL(string: "VideoApp://video/\(i)");
//        contentItem.playURL = NSURL(string: "VideoApp://video/\(i)");
//      }
//      ContentItems.append(contentItem)
//    }
    
    // Section Details
//    wrapperItem.title = "Featured Movies"
//    wrapperItem.topShelfItems = ContentItems
//    
//    return [wrapperItem]
  }

}

